ALTER TABLE `files` ADD `is_public` INT(0) NOT NULL AFTER `id_user`;
ALTER TABLE `files` ADD `is_folder` INT(0) NOT NULL AFTER `is_public`;