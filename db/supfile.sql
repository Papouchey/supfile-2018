-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 06 Juin 2018 à 14:54
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `supfile`
--

-- --------------------------------------------------------

--
-- Structure de la table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `path` varchar(600) NOT NULL,
  `level` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `is_public` int(11) NOT NULL,
  `is_folder` int(11) NOT NULL,
  `token_public` varchar(160) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `firstname` varchar(120) NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(150) NOT NULL,
  `creation_date` datetime NOT NULL,
  `token` varchar(200) DEFAULT NULL,
  `iscsi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `lastname`, `firstname`, `username`, `email`, `password`, `creation_date`, `token`, `iscsi`) VALUES
(8, 'CUVELIER', 'Thomas', 'papuche', 'tcuvelier@atecna.fr', '$2a$08$UAW7frpFrdTbOMPQTGJeMeyt7.SoAamD02pDgRUbICKhT166HFnMa', '2018-05-20 12:49:40', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwiaWF0IjoxNTI4Mjk1NTY5LCJleHAiOjE1MjgzODE5Njl9.C38hXM0F_WISx7ANHUy6yQ90llEM4gjssMezAqyphRE', 1),
(13, 'CUVELIER', 'Thomas', 'thomas', '204559@supinfo.com', '$2a$08$JaMRFfCSoF3jyNspGDvQOu0LhDyUCcKSQK.SZMj896AepWXJwmeXm', '2018-06-06 16:53:27', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTMsImlhdCI6MTUyODI5NjgzNCwiZXhwIjoxNTI4MzgzMjM0fQ.pU7StDb8n-jTiiN_puMEBQwF6UWfL9-ND_f29wQHRwU', 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
