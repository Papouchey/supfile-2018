var express = require('express');
    cors = require('cors'),
    fs = require('fs'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bcrypt = require('bcryptjs'),
    bodyParser = require('body-parser'),
    fileUpload = require('express-fileupload'),
    request = require("request"),
    http = require('http'),
    auth = require('basic-auth'),
    getSize = require('get-folder-size'),
    zipFolder = require('zip-folder'),
    rimraf = require('rimraf');
var app = express();

/**
 * CLASSES
 */
var module_user = require('./classes/user');
var module_file = require('./classes/files');
var CONSTANTS = require('./config');


/**
 * Import Modules
 */



var originsWhitelist = [
    'http://localhost:8100',      //this is my front-end url for development

    'http://www.myproductionurl.com'
];

//here is the magic
app.use(cors());

/**
 * DB connection
 */
var contents = fs.readFileSync("config.json");
// Define to JSON type
var jsonContent = JSON.parse(contents);


// Nous définissons ici les paramètres du serveur.
var hostname = 'localhost';
var port = 3000;

app.use(fileUpload());
app.use(bodyParser.json());

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

app.use('/api', router);

router.get('/test', function(req, res) {
    res.sendStatus(200);
});
//Methode de login
router.route('/login')
    .post(function(req, res) {
        var user = new module_user.User();
        user.connection(req.body, function(firstname, lastname, email, username, id, token) {
            res.json({
                firstname: firstname,
                lastname: lastname,
                id: id,
                email:email,
                username: username,
                token: token
            });

        }, function() {
            res.sendStatus(403);
        });
    });

router.route('/user')
    //Récupère un utilisateur
    .get(function (req, res) {
        if (req.query.id_user === undefined) {
            res.sendStatus(503);
            return;
        }
            var user = new module_user.User();
            user.getUser(req.query, function(firstname, lastname, email, username){
                res.json({
                    firstname: firstname,
                    lastname: lastname,
                    email: email,
                    username: username
                });
            });

    })
    //Crée un utilisateur
    .post(function (req, res) {
        var user = new module_user.User();
        if (req.body.email === undefined || req.body.lastname === undefined ||
            req.body.username === undefined || req.body.password === undefined || req.body.firstname === undefined) {
            res.sendStatus(503);
            return;
        }
        user.insertUser(req.body, function(retour, iscsi) {
            if(retour === "email" ){
                res.json('Email already taken');
            } else if(retour === "username"){
                res.json('Username already taken');
            } else {
                fs.mkdirSync(CONSTANTS.PATH+iscsi+'/'+req.body.username);
                res.json('Oki');
            }
        });

    })
    //Met à jour un utilisateur
    .put(function (req, res) {
        var user = new module_user.User();
        user.updateUser(req.body, function(firstname, lastname, email, username, id, token, bool){
            if(bool === 0){
                res.json({
                    firstname: firstname,
                    lastname: lastname,
                    id: id,
                    email:email,
                    username: username,
                    token: token,
                    response: 'Email already taken'
                });
            } else if (bool === 1) {
                res.json({
                    firstname: firstname,
                    lastname: lastname,
                    id: id,
                    email:email,
                    username: username,
                    token: token,
                    response: 'Oki'
                });
            }
        });
    })
    .delete(function (req, res) {
    });

//Récupère les fichiers
router.get('/get-documents', function(req, res) {
    var user = new module_user.User();
    user.verifyToken(req.query.id_user, req.query.token, function(rows) {
        if (rows.length > 0) {
            var file = new module_file.Files();
            file.getDocuments(req.query, function (data, err) {
                if (err)
                    return res.status(500).send(err);
                res.send(JSON.stringify(data));
            });
        } else {
            res.sendStatus(503);
        }
    });
});

// Récupère les dossiers sauf celui où l'on se trouve
router.get('/get-foldersExcept', function(req, res) {
    var user = new module_user.User();
    user.verifyToken(req.query.id_user, req.query.token, function(rows) {
        if (rows.length > 0) {
            var file = new module_file.Files();
            file.getFoldersExcept(req.query, function (data, err) {
                if (err)
                    return res.status(500).send(err);
                res.send(JSON.stringify(data));
            });
        } else {
            res.sendStatus(503);
        }
    });
});

// Récupère les dossiers
router.get('/get-folders', function(req, res) {
    var user = new module_user.User();
    user.verifyToken(req.query.id_user, req.query.token, function(rows) {
        if (rows.length > 0) {
            var file = new module_file.Files();
            file.getFolders(req.query, function (data, err) {
                if (err)
                    return res.status(500).send(err);
                res.send(JSON.stringify(data));
            });
        } else {
            res.sendStatus(503);
        }
    });
});

//
router.route('/file/:name')
    // SEND IMAGE/AUDIO/VIDEO FOR PREVIEW (
    .get(function (req, res, next) {
        var user = new module_user.User();
        user.verifyToken(req.query.id_user, req.query.token, function(rows) {
            if (rows.length > 0) {
                var iscsi = rows[0].iscsi;
                var options = {
                    root: CONSTANTS.PATH+iscsi+'/' +req.query.path,
                    dotfiles: 'deny',
                    headers: {
                        'x-timestamp': Date.now(),
                        'x-sent': true
                    }
                };
                var fileName = req.params.name;
                res.sendFile(fileName, options, function (err) {
                    if (err) {
                        next(err);
                    }
                });
            } else {
                res.sendStatus(503);
            }
        });
    })
    //Supprime un fichier
    .delete(function (req, res) {
        var user = new module_user.User();
        var file = new module_file.Files();
        var fname = req.params.name;
        user.verifyToken(req.query.id_user, req.query.token, function(rows) {
            if (rows.length > 0) {
                var iscsi = rows[0].iscsi;
                file.verifyDocumentOwner(req.query.path, req.query.id_user, fname, function(result) {
                    if (result === 1) {
                        file.removeFile(req.query.path, fname, req.query.id_user);
                        if (fs.existsSync(CONSTANTS.PATH+iscsi+'/'+req.query.path+"/"+fname)) {
                            var stats = fs.lstatSync(CONSTANTS.PATH+iscsi+'/'+req.query.path+"/"+fname);
                            if (stats.isDirectory()) {
                                rimraf(CONSTANTS.PATH+iscsi+'/'+req.query.path+fname, function() {
                                    file.removePath(req.query.path+fname+'/', req.query.id_user);
                                });
                            } else {
                                fs.unlink(CONSTANTS.PATH+iscsi+'/'+req.query.path+"/"+fname);
                            }
                            res.send("File deleted");
                        }
                    } else {
                        res.sendStatus(503);
                    }
                });
            }
        });
    })
    //Renommer un fichier/dossier
    .put(function(req, res) {
        var user = new module_user.User();
        var file = new module_file.Files();
        var oldFile;
        var newFile;
        user.verifyToken(req.body.id_user, req.body.token, function (rows) {
            if (rows.length > 0) {
                var iscsi = rows[0].iscsi;
                if (req.body.extension === '.undefined'){
                    oldFile = req.body.old_fileName;
                    newFile = req.body.new_fileName;
                } else {
                    oldFile = req.body.old_fileName + req.body.extension;
                    newFile = req.body.new_fileName + req.body.extension;
                }
                file.verifyDocumentOwner(req.body.path, req.body.id_user, oldFile, function (result) {
                    if (result === 1) {
                        file.testIfFolder(req.body.path, req.body.id_user, oldFile, function(result2){
                            if (result2 === 1){
                                file.updateFolderName(req.body, function(response) {
                                    if (response === "ok") {
                                        if(fs.existsSync(CONSTANTS.PATH+iscsi+'/'+req.body.path+"/"+oldFile)){
                                            fs.rename(CONSTANTS.PATH+iscsi+'/'+req.body.path+"/"+oldFile, CONSTANTS.PATH+iscsi+'/'+req.body.path+"/"+newFile);
                                            res.json({
                                                response: 'Oki'
                                            });
                                        }
                                    } else {
                                        res.json({
                                            response: 'Folder already exist'
                                        });
                                    }
                                });
                            } else {
                                file.updateFileName(req.body, function (response) {
                                    if (response === "ok") {
                                        if(fs.existsSync(CONSTANTS.PATH+iscsi+'/'+req.body.path+"/"+oldFile)){
                                            fs.rename(CONSTANTS.PATH+iscsi+'/'+req.body.path+"/"+oldFile, CONSTANTS.PATH+iscsi+'/'+req.body.path+"/"+newFile);
                                            res.json({
                                                response: 'Oki'
                                            });
                                        }
                                    } else {
                                        res.json({
                                            response: 'File already exist'
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        res.sendStatus(503);
                    }
                });
            } else {
                res.sendStatus(503);
            }

        });
    });



router.route('/moveFile/:name')
    .put(function(req, res) {
        var user = new module_user.User();
        var file = new module_file.Files();
        user.verifyToken(req.body.id_user, req.body.token, function (rows) {
            if (rows.length > 0) {
                var iscsi = rows[0].iscsi;
                file.verifyDocumentOwner(req.body.oldPath, req.body.id_user, req.params.name, function (result) {
                    if (result === 1) {
                        file.testIfFolder(req.body.oldPath, req.body.id_user, req.params.name, function(result2){
                            if (result2 === 1){
                                file.updateFolderPath(req.body, function(response) {
                                    if (response === "ok") {
                                        if(fs.existsSync(CONSTANTS.PATH+iscsi+'/'+req.body.oldPath+"/"+req.params.name)){
                                            fs.rename(CONSTANTS.PATH+iscsi+'/'+req.body.oldPath+"/"+req.params.name, CONSTANTS.PATH+iscsi+'/'+req.body.newPath+"/"+req.params.name);
                                            res.json({
                                                response: 'Oki'
                                            });
                                        }
                                    } else {
                                        res.json({
                                            response: 'Folder already exist'
                                        });
                                    }
                                });
                            } else {
                                file.updateFilePath(req.body, function(response) {
                                    if (response === "ok") {
                                        if(fs.existsSync(CONSTANTS.PATH+iscsi+'/'+req.body.oldPath+"/"+req.params.name)){
                                            fs.rename(CONSTANTS.PATH+iscsi+'/'+req.body.oldPath+"/"+req.params.name, CONSTANTS.PATH+iscsi+'/'+req.body.newPath+"/"+req.params.name);
                                            res.json({
                                                response: 'Oki'
                                            });
                                        }
                                    } else {
                                        res.json({
                                            response: 'File already exist'
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        res.sendStatus(503);
                    }
                });
            } else {
                res.sendStatus(503);
            }

        });
    });



// DOWNLOAD FILE
router.get('/download/:name', function (req, res, next) {
    var user = new module_user.User();
    var file = new module_file.Files();
    var fname = req.params.name;
    user.verifyToken(req.query.id_user, req.query.token, function(rows) {
        if (rows.length > 0) {
            var iscsi = rows[0].iscsi;
            file.verifyDocumentOwner(req.query.path, req.query.id_user, fname, function(result) {
                if (result === 1) {
                    file.testIfFolder(req.query.path, req.query.id_user, fname, function(result2) {
                        if (result2 === 1) {
                            var rand = Math.random().toString(36).substr(2);
                            if( !fs.existsSync(CONSTANTS.PATH+iscsi+'/zip-'+req.query.id_user)) {
                                fs.mkdirSync(CONSTANTS.PATH+iscsi+'/zip-'+req.query.id_user);
                            }
                            zipFolder(CONSTANTS.PATH+iscsi+'/'+req.query.path+'/'+fname, CONSTANTS.PATH+iscsi+'/zip-'+req.query.id_user+'/'+rand+'.zip', function() {
                                var fileLocation = path.join(CONSTANTS.PATH+iscsi+'/zip-'+req.query.id_user+'/', rand+'.zip');
                                res.download(fileLocation, fname+'.zip');
                            });
                        } else {
                            var fileLocation = path.join(CONSTANTS.PATH+iscsi+'/'+req.query.path, fname);
                            res.download(fileLocation, fname);
                        }
                    });

                } else {
                    res.sendStatus(503);
                }
            });
        } else {
            res.sendStatus(503);
        }
    });
});

// DOWNLOAD FILE PUBLIC
router.get('/download/:username/:token_public', function (req, res, next) {
    var user = new module_user.User();
    var file = new module_file.Files();
    file.getInfoWithTokenPublic(req.params.username, req.params.token_public, function(resultUser, resultFile) {
        var iscsi = resultUser.iscsi;
        var path2 = resultFile.path.substr(1, resultFile.path.length-1);
       file.testIfFolder(path2, resultUser.id, resultFile.name, function(resultat) {
           if (resultat === 1) {
                var rand = Math.random().toString(36).substr(2);
                if( !fs.existsSync(CONSTANTS.PATH+iscsi+"/zip-" + resultUser.id)) {
                    fs.mkdirSync(CONSTANTS.PATH+iscsi+"/zip-" + resultUser.id);
                }
                zipFolder(CONSTANTS.PATH+iscsi+ '/' + path2 + '/' + resultFile.name, CONSTANTS.PATH+iscsi+"/zip-" + resultUser.id + '/' + rand + '.zip', function() {
                    var fileLocation = path.join(CONSTANTS.PATH+iscsi+ '/zip-' + resultUser.id + '/', rand + '.zip');
                    res.download(fileLocation, resultFile.name + '.zip');
                });
           } else {
               var fileLocation = path.join(CONSTANTS.PATH+iscsi+'/'+path2, resultFile.name);
               res.download(fileLocation, resultFile.name);
           }
       });
    });
});

//Upload un fichier
router.post('/upload', function(req, res) {
    if (!req.files)
        return res.status(400).send('No files were uploaded.');
    var user = new module_user.User();
    user.verifyToken(req.body.id_user, req.body.token, function(rows) {
        if (rows.length > 0) {
            var iscsi = rows[0].iscsi;
            // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
            var sampleFile = req.files.file;

            if( !fs.existsSync(CONSTANTS.PATH+iscsi+'/'+req.body.path)) {
                fs.mkdirSync(CONSTANTS.PATH+iscsi+'/'+req.body.path);
            }

            var file = new module_file.Files();
            file.createFile(sampleFile.name, sampleFile.name.split('.').pop(), req.body, function(req2) {
                if (req2 ==="ok"){
                    // Use the mv() method to place the file somewhere on your server
                    sampleFile.mv(CONSTANTS.PATH+iscsi+'/'+req.body.path+sampleFile.name, function(err) {
                        if (err) return res.status(500).send(err);
                        res.send('upload');
                    });
                } else {
                    res.send('File already exist');
                }
            });
        } else {
            res.sendStatus(503);
        }
    });
});

//Crée un dossier
router.post('/directory', function(req, res, next) {
    var user = new module_user.User();
    user.verifyToken(req.body.id_user, req.body.token, function(rows) {
        if (rows.length > 0) {
            var iscsi = rows[0].iscsi;

            if( !fs.existsSync(CONSTANTS.PATH+iscsi+'/'+req.body.path)) {
                fs.mkdirSync(CONSTANTS.PATH+iscsi+'/'+req.body.path);
            }

            var file = new module_file.Files();
            file.createFile(req.body.name, '', req.body, function(req2) {
                if (req2 ==="ok"){
                    fs.mkdir(CONSTANTS.PATH+iscsi+'/'+req.body.path+req.body.name, function(err) {
                        res.send('create');
                    });
                } else {
                    res.send('Directory already exist');
                }
            });
        } else {
            res.sendStatus(503);
        }

    });
});


//Récupère la taille du dossier utilisateur
router.route('/directory/:name')
    .get(function(req, res) {
        var username = req.params.name;
        var user = new module_user.User();
        user.verifyToken(req.query.id_user, req.query.token, function(rows){
            if (rows.length > 0) {
                var iscsi = rows[0].iscsi;
                getSize(CONSTANTS.PATH+iscsi+'/'+username, function(err, size){
                    res.json({
                        size: (size / 1024 / 1024).toFixed(2)
                    });
                });
            }
        });
    })


router.route('/IsPublicFile/:name')
    // Permet de savoir si un fichier/dossier est public
    .get(function(req, res) {
        var user = new module_user.User();
        var file = new module_file.Files();
        user.verifyToken(req.query.id_user, req.query.token, function (rows) {
            file.verifyDocumentOwner(req.query.path, req.query.id_user, req.params.name, function (result) {
                if (result === 1) {
                    file.getTokenPublic(req.query, req.params.name, function(resultat1, resultat2) {
                        res.json({
                            token_public: resultat1[0].token_public,
                            username: resultat2[0].username,
                        });
                    })
                } else {
                    res.sendStatus(503);
                }
            });
        });
    })
    //Modifie le fichier/dossier en public/privé
    .put(function(req, res) {
        var user = new module_user.User();
        var file = new module_file.Files();
        user.verifyToken(req.body.id_user, req.body.token, function (rows) {
            file.verifyDocumentOwner(req.body.path, req.body.id_user, req.params.name, function (result) {
                if (result === 1) {
                    file.setPublic(req.body, function(resultat) {
                        res.json({
                            response: 'Oki'
                        });
                    })
                } else {
                    res.sendStatus(503);
                }
            });
        });
    });


router.route('/search')
    .get(function(req, res){
        var user = new module_user.User();
        var file = new module_file.Files();
        user.verifyToken(req.query.id_user, req.query.token, function (rows) {
            if (rows.length > 0) {
                file.search(req.query, function (result) {
                    res.json({
                        result: result
                    });
                });
            } else {
                res.sendStatus(503);
            }
        });
    });

router.route('/getTxt')
    .get(function(req, res) {
        var user = new module_user.User();
        user.verifyToken(req.query.id_user, req.query.token, function (rows) {
            if (rows.length > 0) {
                var data = fs.readFileSync('uploads/iscsi' + rows[0].iscsi +'/' + req.query.path + req.query.name, 'utf8');
                res.json({
                    result: data
                });
            } else {
                res.sendStatus(503);
            }
        });
    });


app.use(router);
var server = app.listen(CONSTANTS.PORT, function () {
    console.log("Example app listening at http://%s:%s", hostname, port)
});












