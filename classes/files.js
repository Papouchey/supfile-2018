var mysql = require("mysql");
var jwt = require('jsonwebtoken');
var CONSTANTS = require('../config');

var con = mysql.createConnection({
    host: CONSTANTS.HOST_SQL,
    user: CONSTANTS.USER_DB,
    password: CONSTANTS.PASSWORD_DB,
    database: CONSTANTS.DATABASE
});


module.exports = {
    Files: function User()
    {
        this.id = "";
        this.name = "";
        this.extension = "";
        this.path = "";
        this.level = "";
        this.id_user = "";
        this.is_public = "";
        this.is_folder = "";
        this.creation_date = "";
        this.update_date = "";
        this.token_public = "";


        //Récupère les fichiers au niveau donnée
        this.getDocuments = function (informations, callback) {
            con.query('SELECT * FROM files WHERE id_user = '+informations.id_user+' AND level = '+informations.level+' AND path LIKE \'%'+informations.path+'\' ORDER BY is_folder DESC, name ASC', function(err,rows){
                if(err) throw err;
                var result = new Array();
                for (var i = 0; i < rows.length; i++) {
                    result.push(rows[i]);
                }
                callback(result);
            });
        };

        this.getFolders = function (informations, callback) {
            con.query('SELECT * FROM files WHERE id_user = '+informations.id_user+' AND is_folder = 1  ORDER BY path ASC, name ASC', function(err,rows){
                if(err) throw err;
                var result = new Array();
                for (var i = 0; i < rows.length; i++) {
                    result.push(rows[i]);
                }
                callback(result);
            });
        };

        this.getFoldersExcept = function (informations, callback) {
            con.query('SELECT * FROM files WHERE id_user = '+informations.id_user+' AND is_folder = 1  AND (path <> \'/' + informations.path + '\' OR name <> \'' + informations.name + '\') ORDER BY path ASC, name ASC', function(err,rows){
                if(err) throw err;
                con.query('SELECT * FROM users WHERE id = '+informations.id_user, function(err2, rows2){
                    if(err2) throw err2;
                    var result = new Array();
                    for (var i = 0; i < rows.length; i++) {
                        result.push(rows[i]);
                    }
                    var racine = {
                        id: -1,
                        name: rows2[0].username,
                        extension: '',
                        path: '/' + rows2[0].username,
                        level: 0,
                        id_user: informations.id_user,
                        is_public: 1,
                        is_folder: 1,
                        creation_date: 0,
                        update_date: 0
                    };
                    result.push(racine);
                    callback(result);
                });
            });
        };

        //Create a file in db
        this.createFile = function (file, extension, informations, callback) {
            con.query('SELECT * FROM files WHERE id_user =' + informations.id_user + ' AND name LIKE \'' + file + '\' AND path like \'/' + informations.path + '\'', function (err, rows) {
                if (err)  throw err;
                if (rows.length > 0) {
                    callback("File already exist");
                } else {
                    var token_public = '';
                    if (informations.is_public) {
                        token_public = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
                    }
                    con.query('INSERT INTO files (name, extension, path, level, id_user, is_public, is_folder, token_public, creation_date, update_date) VALUES ' +
                        '(\'' + file + '\', \'' + extension + '\', \'/' + informations.path + '\', ' + informations.level +
                        ', ' + informations.id_user + ',' + informations.is_public + ',' + informations.is_folder + ', \''+token_public+'\', NOW(), NOW());', function (err, rows) {
                        if (err) throw err;
                        callback("ok");
                    });
                }
            });
        };

        //remove file from db
        this.removeFile = function (fileLocation, fname, id_user) {
            con.query('DELETE FROM files WHERE id_user = '+id_user+' AND path LIKE \'/'+fileLocation+'\' AND name LIKE \''+fname+'\'', function(err,rows){
                if(err) throw err;
            });
        };

        this.removePath = function (fileLocation, id_user) {
            con.query('DELETE FROM files WHERE id_user = '+id_user+' AND path LIKE \'/'+fileLocation+'%\'', function(err,rows){
                if(err) throw err;
            });
        }



        this.updateFileName = function (informations, callback){
            con.query('SELECT * FROM files WHERE id_user ='+informations.id_user+' AND path LIKE \'/'+informations.path+'\' AND name LIKE \'' + informations.new_fileName + informations.extension + '\'', function(err, rows){
                if(err) throw err;
                if (rows.length > 0) {
                    callback("File already exist");
                } else {
                    con.query('UPDATE files SET name = \'' + informations.new_fileName + informations.extension + '\' WHERE name LIKE \'' + informations.old_fileName + informations.extension + '\' AND path LIKE \'/' + informations.path + '\' AND id_user = '+informations.id_user, function(err2, rows2){
                        if(err2) throw err2;
                        callback("ok");
                    });
                }
            });
        };

        this.updateFolderName = function (informations, callback) {
            con.query('SELECT * FROM files WHERE id_user ='+informations.id_user+' AND path LIKE \'/'+informations.path+'\' AND name LIKE \'' + informations.new_fileName + '\'', function(err, rows){
                if(err) throw err;
                if (rows.length > 0) {
                    callback("Folder already exist");
                } else {
                    con.query('UPDATE files SET name = \'' + informations.new_fileName + '\' WHERE name LIKE \'' + informations.old_fileName + '\' AND path LIKE \'/' + informations.path + '\' AND id_user = '+informations.id_user, function(err2, rows2){
                        if(err2) throw err2;
                       con.query('UPDATE files SET path = REPLACE(path, \'/' + informations.path + informations.old_fileName + '\', \'/' + informations.path + informations.new_fileName + '\') WHERE path LIKE \'/' + informations.path + '%\' AND id_user = '+informations.id_user, function(err3, rows3){
                            if (err3) throw err3;
                            callback("ok");
                        });
                    });
                }
            });
        };



        this.updateFilePath = function (informations, callback) {
            con.query('SELECT * FROM files WHERE id_user =' + informations.id_user+' AND path LIKE \''+informations.newPath+'/\' AND name LIKE \'' + informations.name + informations.extension + '\'', function(err, rows){
                if(err) throw err;
                if (rows.length > 0) {
                    callback("File already exist");
                } else {
                    con.query('UPDATE files SET path = \''+informations.newPath+'/\', level = ' + informations.level + ' WHERE name LIKE \'' + informations.name + informations.extension + '\' AND path LIKE \'/' + informations.oldPath + '\' AND id_user = '+informations.id_user, function(err2, rows2){
                        if(err2) throw err2;
                        callback("ok");
                    });
                }
            })
        };

        this.updateFolderPath = function (informations, callback) {
            con.query('SELECT * FROM files WHERE id_user ='+informations.id_user+' AND path LIKE \''+informations.newPath+'/\' AND name LIKE \'' + informations.name + '\'', function(err, rows){
                if(err) throw err;
                if (rows.length > 0) {
                    callback("Folder already exist");
                } else {
                    con.query('UPDATE files SET path = \'' + informations.newPath + '/\', level = ' + informations.level + ' WHERE name LIKE \'' + informations.name + '\' AND path LIKE \'/' + informations.oldPath + '\' AND id_user = '+informations.id_user, function(err2, rows2) {
                        if(err2) throw err2;
                        var levelFile = informations.level + 1;
                        con.query('UPDATE files SET path = REPLACE(path, \'/' + informations.oldPath + informations.name + '\', \'' + informations.newPath + '/' +informations.name + '\'), level = ' + levelFile + ' WHERE path LIKE \'/' + informations.oldPath   + informations.name + '%\' AND id_user = '+informations.id_user, function(err3, rows3){
                            if (err3) throw err3;
                            callback("ok");
                        });
                    });
                }
            });
        };


        //Verify if user is owner of document
        this.verifyDocumentOwner = function(path, id_user, fname, callback) {
            con.query('SELECT * FROM files f WHERE f.id_user = '+id_user+' AND f.path LIKE \'/'+path+'\' AND f.name LIKE \''+fname+'\'', function(err,rows){
                if(err) throw err;
                if (rows.length > 0) {
                    callback(1);
                } else {
                    callback(0);
                }
            });
        };

        this.testIfFolder = function(path, id_user, fname, callback) {
            con.query('SELECT is_folder FROM files f WHERE f.id_user = '+id_user+' AND f.path LIKE \'/'+path+'\' AND f.name LIKE \''+fname+'\' AND is_folder = 1', function(err,rows){
                if(err) throw err;
                if (rows.length > 0) {
                    callback(1);
                } else {
                    callback(0);
                }
            });
        };

        this.setPublic = function(informations, callback) {
            var token_public = '';
            if (informations.is_public) {
                token_public = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
            }
            con.query('UPDATE files SET is_public = '+informations.is_public+', token_public = \''+token_public+'\' WHERE name LIKE \'' + informations.name + '\' AND path LIKE \'/' + informations.path + '\' AND id_user = '+informations.id_user, function(err2, rows2) {
                callback('Oki');
            });
        };

        this.getTokenPublic = function(informations, fname, callback) {
            con.query('SELECT * FROM files WHERE id_user='+informations.id_user+' AND name LIKE \'' + fname + '\' AND path LIKE \'/' + informations.path + '\'', function(err, rows) {
                if(err) throw err;
                con.query('SELECT * FROM users WHERE id = '+informations.id_user, function(err2, rows2){
                    callback(rows, rows2);
                });
            });
        }

        this.getInfoWithTokenPublic = function(username, token_public, callback) {
            con.query('SELECT * FROM users WHERE username = \''+username+'\'', function(err, rows){
                if(err) throw err;
                con.query('SELECT * FROM files WHERE token_public = \'' + token_public + '\' AND id_user = ' + rows[0].id, function (err2, rows2) {
                    callback(rows[0], rows2[0])
                });
            });
        };

        this.search = function(informations, callback) {
            con.query('SELECT * FROM files WHERE id_user = ' + informations.id_user + ' AND name like \'%' + informations.name + '%\' ORDER BY is_folder ASC, path ASC, name ASC', function (err, rows) {
                if (err) throw err;
                callback(rows);
            });
        };
    }
};
