var mysql = require("mysql");
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var CONSTANTS = require('../config');

var con = mysql.createConnection({
    host: CONSTANTS.HOST_SQL,
    user: CONSTANTS.USER_DB,
    password: CONSTANTS.PASSWORD_DB,
    database: CONSTANTS.DATABASE
});


var result = {'pseudo': '', 'email': ''};

module.exports = {
    User: function User() {
        this.id = "";
        this.lastname = "";
        this.firstname = "";
        this.email = "";
        this.password = "";
        this.username = "";
        this.creation_date = "";
        this.iscsi = "";


        this.getUser = function (informations, callback) {
            con.query('SELECT * FROM users WHERE id = '+informations.id_user, function(err,rows){
                if(err) throw err;
                callback(rows[0].firstname, rows[0].lastname, rows[0].email, rows[0].username);
            });
        };


        //Insert user into DB
        this.insertUser = function (informations, callback) {
            var iscsi = 1;
            con.query('SELECT * FROM users WHERE email LIKE \'' + informations.email + "'", function (err, rows) {
                if (err) throw err;
                result.email = rows;

                con.query('SELECT * FROM users WHERE username LIKE \'' + informations.username + "'", function (err, rows) {
                    if (err) throw err;
                    result.username = rows;
                    if (result.username.length > 0) {
                        var retour = "username";
                        callback(retour, iscsi);
                    } else if (result.email.length > 0) {
                        var retour = "email";
                        callback(retour, iscsi);
                    } else {
                        con.query('SELECT iscsi FROM users WHERE id = (SELECT MAX(id) FROM users)', function (err, rows) {
                            iscsi = ((rows[0].iscsi)%3)+1;
                            con.query('INSERT INTO users (email, lastname, firstname, username, password, creation_date, iscsi) VALUES (\'' + informations.email + '\', \'' + informations.lastname + '\', \'' + informations.firstname + '\', \'' + informations.username + '\', \'' + bcrypt.hashSync(informations.password, 8) + '\', NOW(),' +iscsi+');', function (err, rows) {
                                if (err) throw err;
                                var retour = "ok";
                                callback(retour, iscsi);
                            });
                        });
                    }
                });

            });

        };


        this.updateUser = function (informations, callback) {
            con.query('SELECT * FROM users WHERE id = '+informations.id, function(err,rows){
                if(err) throw err;
                if (rows[0].email === informations.email) {
                    var bool = 1;
                    if (informations.password === '') {
                        con.query('UPDATE users SET lastname=\''+informations.lastname+'\',firstname=\''+informations.firstname+'\' WHERE id = '+rows[0].id, function(err2, rows2){
                            callback(informations.firstname, informations.lastname, rows[0].email, rows[0].username, rows[0].id, rows[0].token, bool);
                        });
                    } else {
                        con.query('UPDATE users SET lastname=\''+informations.lastname+'\',firstname=\''+informations.firstname+'\',password=\''+bcrypt.hashSync(informations.password,8)+'\' WHERE id = '+rows[0].id, function(err2, rows2){
                            callback(informations.firstname, informations.lastname, rows[0].email, rows[0].username, rows[0].id, rows[0].token, bool);
                        });
                    }
                } else {
                    con.query('SELECT * FROM users WHERE email LIKE \'' + informations.email + "'", function (err3, rows3) {
                        if (err3) throw err3;
                        result.email = rows3;
                        if (result.email.length > 0) {
                            var bool = 0;
                            callback(rows[0].email, rows[0].username, rows[0].id, rows[0].token, bool);
                        } else {
                            var bool = 1;
                            if (informations.password === '') {
                                con.query('UPDATE users SET lastname=\''+informations.lastname+'\',firstname=\''+informations.firstname+'\',email=\''+informations.email+'\' WHERE id = '+rows[0].id, function(err4, rows4){
                                    callback(informations.firstname, informations.lastname, informations.email, rows[0].username, rows[0].id, rows[0].token, bool);
                                });
                            } else {
                                con.query('UPDATE users SET lastname=\''+informations.lastname+'\',firstname=\''+informations.firstname+'\',email=\''+informations.email+'\',password=\''+bcrypt.hashSync(informations.password,8)+'\' WHERE id = '+rows[0].id, function(err5, rows5){
                                    callback(informations.firstname, informations.lastname, informations.email, rows[0].username, rows[0].id, rows[0].token, bool);
                                });
                            }
                        }
                    });
                }
            });
        };


        //User can connect to database
        this.connection = function (informations, callback, erreur_login) {
            bcrypt.hashSync(informations.password);
            con.query('SELECT * FROM users WHERE (email LIKE \'' + informations.email + '\' OR username LIKE \'' + informations.email + '\')', function (err, rows) {
                if (err) throw err;
                if (rows.length > 0) {
                    if (bcrypt.compareSync(informations.password, rows[0].password)) {
                        var token = jwt.sign({id: rows[0].id}, CONSTANTS.SECRET, {
                            expiresIn: 86400 // expires in 24 hours
                        });
                        con.query('UPDATE users SET token = \'' + token + '\' WHERE id = ' + rows[0].id, function (err, rows2) {
                            callback(rows[0].firstname, rows[0].lastname, rows[0].email, rows[0].username, rows[0].id, token);
                        });
                    } else {
                        erreur_login();
                    }
                } else {
                    erreur_login();
                }

            });
        };

        this.verifyToken = function (id_user, token, callback) {
            con.query('SELECT * FROM users WHERE (id = ' + id_user + ' AND token LIKE \'' + token + '\')', function (err, rows) {
                if (err) throw err;
                callback(rows);
            });
        }
    }
};
